document.addEventListener('DOMContentLoaded', function() {
    const gameArea = document.querySelector('.game-area');
    const chests = document.querySelectorAll('.chest');
    const modal = document.querySelector('.modal');
    const infoText = document.getElementById('infoText');
    const close = document.querySelector('.close');
    const canvas = document.querySelector('.character');
    const ctx = canvas.getContext('2d');

    // Hero tileset
    const heroImg = document.getElementById('heroImg');

    let heroFrame = 0;
    let heroDirection = 'right';

    // Função para posicionar os baús aleatoriamente
    function positionChests() {
        chests.forEach(chest => {
            const x = Math.random() * (gameArea.clientWidth - chest.clientWidth);
            const y = Math.random() * (gameArea.clientHeight - chest.clientHeight);
            chest.style.left = x + 'px';
            chest.style.top = y + 'px';
        });
    }

    positionChests(); // Posiciona os baús aleatoriamente ao carregar a página

    // Função para atualizar a posição do herói
    function updateHero() {
        // Adicione aqui a lógica para mover o herói com base nas teclas pressionadas
    }

    // Função para desenhar o herói no canvas
    function drawHero() {
        const frameWidth = 64;
        const frameHeight = 64;
        const frameX = heroFrame * frameWidth;
        const frameY = 0;

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(heroImg, frameX, frameY, frameWidth, frameHeight, 0, 0, frameWidth, frameHeight);

        heroFrame = (heroFrame + 1) % 4; // Atualiza o frame do herói
    }

    // Game loop
    function gameLoop() {
        updateHero();
        drawHero();
        requestAnimationFrame(gameLoop);
    }

    gameLoop(); // Inicia o game loop

    chests.forEach(chest => {
        chest.addEventListener('click', function() {
            infoText.textContent = this.getAttribute('data-info');
            modal.style.display = 'block';
        });
    });

    close.addEventListener('click', function() {
        modal.style.display = 'none';
    });

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = 'none';
        }
    };
});
