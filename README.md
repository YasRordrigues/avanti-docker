### Avanti-docker

## Mundo DevOps

**Mundo DevOps** foi desenvolvido como parte de um desafio do bootcamp Devops da Avanti para demonstrar habilidades na configuração de um pipeline de CI/CD utilizando Docker e GitLab CI. O projeto consiste em uma página web simples feita com HTML, CSS e JavaScript de um jogo educativo, onde o jogador interage com baús que revelam informações sobre ferramentas DevOps como Git, Docker, Kubernetes, Terraform e AWS.


### Pipeline de CI/CD

O pipeline de CI/CD foi configurado no GitLab para automatizar o processo de build, push e deploy da aplicação. A seguir, os principais passos do pipeline:

1. **Build:** Construção da imagem Docker a partir do Dockerfile.
2. **Push:** Upload da imagem Docker para o Docker Hub.
3. **Deploy:** Despliegue do container em um servidor de produção.

### Dockerfile

O Dockerfile define a configuração de um servidor Apache (httpd) que serve a aplicação web. A imagem Docker resultante inclui a aplicação **Mundo DevOps**.

### GitLab CI/CD

O arquivo `.gitlab-ci.yml` define as etapas do pipeline:

- **Build Stage:** Constrói a imagem Docker e faz login no Docker Hub.
- **Push Stage:** Envia a imagem Docker para o Docker Hub.
- **Deploy Stage:** Faz o pull da imagem do Docker Hub e executa o container em um servidor de produção.

### Comandos

Para rodar este projeto, primeiro baixe a imagem do Dockerhub:

`docker pull yasrordrigues/avanti-docker1`

Logo após, rode o container com o Docker:

`docker run -d -p 80:80 --name mundo-devops yasrordrigues/avanti-docker1`

### Desafio

Este projeto foi desenvolvido como parte de um desafio que envolvia criar uma aplicação web simples, configurar um Dockerfile para servir a aplicação, e implementar um pipeline de CI/CD no GitLab para automatizar o build, push e deploy da imagem Docker. 

### Conclusão

O **Mundo DevOps** combina aprendizado e diversão, oferecendo uma maneira interativa de aprender sobre ferramentas DevOps enquanto demonstra habilidades em desenvolvimento web e automação de deploys utilizando Docker e GitLab CI.

---

Desenvolvido por Yasmim Rodrigues para o desafio Avanti Bootcamp DevOps.